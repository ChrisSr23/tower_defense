using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Spawn waves and instantiate enemies
 */
public class WaveSpawner : MonoBehaviour
{
    
    public Wave[] waves;
    public static int wavelength;
    public Transform spawnPoint;
    public int enemyNumber = 5;
    //public float spawnDur = 1.5f;
    public static int waveNumber = 0;
    public static bool wonGame;

    private void Start()
    {
        waveNumber = 0;
        wavelength = waves.Length;
        wonGame = false;
    }
    /**
     * Interaction/Click event to start wave
     */
    public void WaveButtonClick()
    {
       StartCoroutine(SpawnWave()); 
    }

    private void Update()
    {
        GameObject[] enemys = GameObject.FindGameObjectsWithTag("Enemy");
        if ((waveNumber == waves.Length) && (enemys.Length == 0))
        {
            Debug.Log("You Won");
            wonGame = true;
        }
    }

    /**
     * Spawn different waves
     */
    private IEnumerator SpawnWave()
    {
        
        if(waveNumber <= waves.Length - 1)
        {
            Wave wave = waves[waveNumber];
            int counter = 0;
            
            waveNumber += 1;
            
            for (int i = 0; i < wave.count; i++)
            {
                Spawn(wave.enemyPref, 0);
                yield return new WaitForSeconds(wave.spawnRate);

                if (wave.secondEnemyCount > counter)
                {
                    Spawn(wave.secondEnemyPref, 5);
                    counter++;
                    yield return new WaitForSeconds(wave.spawnRate);
                }
            }

         
        }
    }

    /**
     * Instantiate enemy prefab; Offset higher for flying enemies 
     */
    void Spawn(GameObject enemy, int offset)
    {
         
        Instantiate(enemy, spawnPoint.position += new Vector3(0, offset, 0), spawnPoint.rotation);
        //reset spawn position
        spawnPoint.position -= new Vector3(0, offset, 0);
    }
}
