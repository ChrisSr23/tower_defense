using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayRange : MonoBehaviour
{

    public Sprite circleSprite;
    private GameObject circleGameObject;

   /**
  * Create circle around tower to display range
  */
    public void createCircle(float range, Vector3 pos)
    {
        //create sprite
        circleGameObject =  new GameObject("Circle", typeof(SpriteRenderer));
        SpriteRenderer circleSpriteRenderer = circleGameObject.GetComponent<SpriteRenderer>();
        circleSpriteRenderer.sprite = circleSprite;
        circleSpriteRenderer.color = new Color(247 / 255f, 59 / 255f, 97 / 255f, 0.5f);

        Vector3 scale = new Vector3(range, range, range);
        circleSpriteRenderer.transform.localScale = scale;
        // set the position
        Vector3 position = pos;
        circleGameObject.transform.position = position;
        circleGameObject.transform.rotation = Quaternion.Euler(-90, 0f, 0f);
    }

    /**
     * Destroy sprite
     */
    public void deleteCircle()
    {
        Destroy(circleGameObject);
    }

}
