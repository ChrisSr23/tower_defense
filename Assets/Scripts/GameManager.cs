using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    private bool gameEnded;

    public GameObject gameOverUI;
    // Start is called before the first frame update
    void Start()
    {
        gameEnded = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(gameEnded)
        {
            return;
        }
        if (Player.health <= 0)
        {  
            EndGame();
        }
        if (WaveSpawner.wonGame)
        {
            EndGame();
        }
    }

    private void EndGame()
    {
        gameEnded = true;
        gameOverUI.SetActive(true);
    }
}
