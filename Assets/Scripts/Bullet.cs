using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Define bullet behaviour
 */
public class Bullet : MonoBehaviour
{
    private Transform target;

    public float speed = 70f;
    public int bulletDamage = 30;
    public void GetTarget(Transform _target)
    {
        target = _target;
    }


    // Update is called once per frame
    void Update()
    {
        if(target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        //distance that the bullet moves in one frame
        float distance = speed * Time.deltaTime;
        //check if length of the direction(dir) to the target is less than distance that bullet moved in one frame
        //smaller means bullet reached target
        if (dir.magnitude <= distance)
        {
            HitTarget();
            return;
        }

        //normalized -> prevents that speed of bullet changes when target is further afar
        //Ensures constant speed
        transform.Translate(dir.normalized * distance, Space.World);
        //look at enemy
        transform.LookAt(target);
    }

    /**
     * bullet reached enemy
     */
    private void HitTarget() {
        Damage(target);
        //destroy bullet
        Destroy(gameObject);
    }
    /**
     * damage enemy
     */
    private void Damage(Transform target)
    {
        //get current enemy instance
        Enemy e = target.GetComponent<Enemy>();
        if(e != null)
        {   
            //damage enemy
            e.TakeDamage(bulletDamage);
        }
    }


}
