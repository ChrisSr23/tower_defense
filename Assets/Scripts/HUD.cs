using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class HUD : MonoBehaviour
{
   
    public TextMeshProUGUI waveText;
    public TextMeshProUGUI moneyText;
    public TextMeshProUGUI healthText;

    public Button standardButton;
    public Button mageButton;
    public Button waveButton;

    public GameObject panel;

    private int waveNumber;
    public static bool buyPhase;


    // Start is called before the first frame update
    void Start()
    {
        waveText.text = "Welle " + waveNumber.ToString() + "/" + WaveSpawner.wavelength;
        standardButton.interactable = true;
        mageButton.interactable = true;
        waveButton.interactable = true;
        buyPhase = true;
    }

    // Update is called once per frame
    void Update()
    {
      
        waveNumber = WaveSpawner.waveNumber;
        waveText.text = "Welle " + waveNumber.ToString() + "/" + WaveSpawner.wavelength;
       

        moneyText.text = Player.money.ToString() + "$";
        healthText.text = "Leben " + Player.health.ToString();

        
        InteractTowerButtons();
        InteractWaveButton();

    }

    /**
     * Handles interaction with the shop buttons 
     */
    private void InteractTowerButtons()
    {
 
            //disable buttons if user has not enough money
            if (Player.money < BuildHelper.standardCost)
            {
                standardButton.interactable = false;
            }
            else
            {
                standardButton.interactable = true;
            }
            if (Player.money < BuildHelper.mageCost)
            {
                mageButton.interactable = false;
            }
            else
            {
                mageButton.interactable = true;
            }
        
    }

    /**
     * Handles interaction with the wave button 
     */
    private void InteractWaveButton()
    {
        GameObject[] enemys = GameObject.FindGameObjectsWithTag("Enemy");
       
        if ((enemys.Length == 0))
        {
            waveButton.interactable = true;
            buyPhase = true;
        }
        else
        {
            waveButton.interactable = false;
            buyPhase = false;
        }

        panel.gameObject.SetActive(buyPhase);
    }
}
