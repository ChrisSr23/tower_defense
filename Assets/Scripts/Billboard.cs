using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
 * Rotate enemey healthbar to camera
 */
public class Billboard : MonoBehaviour
{

    public Camera cam;

    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.LookAt(transform.position + cam.transform.forward);
    }
}
