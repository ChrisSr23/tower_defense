using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Healthbar of enemies
 */
public class EnemyHealthBar : MonoBehaviour
{
    public Slider enemySlider;

    public void SetMaxHealth(int health)
    {
        enemySlider.maxValue = health;
        enemySlider.value = health;
    }

    public void SetHealth(int health)
    {
        enemySlider.value = health;
    }
}
