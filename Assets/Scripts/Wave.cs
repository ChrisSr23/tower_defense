using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

/**
 * Define different waves in inspector
 */
public class Wave 
{
    public GameObject enemyPref;
    public GameObject secondEnemyPref;
    public int count;
    public int secondEnemyCount;
    public float spawnRate;
}
