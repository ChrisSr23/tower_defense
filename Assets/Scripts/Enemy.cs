using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
/**
 * Define behaviour of enemy
 */
public class Enemy : MonoBehaviour
{
    
    public float speed = 10f;
    public  float rotationSpeed = 7;
    public float damage = 1f;

    public EnemyHealthBar healthBar;
    public int enemyHealth;
    public int enemyMaxHealth = 100;
    public int moneyValue = 50;

    private Transform target;
    private int wavepointIndex = 0;
    private bool isFlying = false;

    void Start()
    {
        target = WayPoints.wayPoints[0];

        healthBar.SetMaxHealth(enemyMaxHealth);
        enemyHealth = enemyMaxHealth;
        //check if enemy is a flying enemey
        if (gameObject.GetComponent("FlyEnemyDummy") != null)
        {
            isFlying = true;
            target = WayPoints.wayPoints[WayPoints.wayPoints.Length - 1];
        }
    }

    // Update is called once per frame
    void Update()
    {
        //direction to waypoint
        Vector3 dir = target.position - transform.position;
        //Time.deldatime so speed does not depend on framerate
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);


        //Rotate enemy to waypoint
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        //convert to "x,y,z - rotation"; "Lerp" for smooth transition from current Position  to new Position (lookRotation)
        Vector3 enemyRotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed).eulerAngles;
        transform.rotation = Quaternion.Euler(0f, enemyRotation.y, 0f);
        //next waypoint if enemy reaches destination
        if (Vector3.Distance(transform.position, target.position) <= 0.2f)
        {
            Debug.Log("Point reached");
            GetNextWayPoint();
        }
    }
    /**
     * Define next waypoint/destination
     */
    private void GetNextWayPoint(){
        //flying enemy has only one destination -> destroy enemy after function is called first time
        if (isFlying) {
            Destroy(gameObject);
            DamagePlayer();
            return;
        }
        //"normal enemy" walks to every single waypoint
        if (wavepointIndex >= WayPoints.wayPoints.Length - 1){
            Destroy(gameObject);
            DamagePlayer();
            return;
        }

        wavepointIndex++;
      
        target = WayPoints.wayPoints[wavepointIndex];
    }
    /**
     * Enemy reaches endpoint
     */
    private void DamagePlayer()
    {
        Player.health -= damage;
    }
    /**
     * Enemy loses health
     */
    public void TakeDamage(int amount)
    {
        enemyHealth -= amount;
        healthBar.SetHealth(enemyHealth);
        if (enemyHealth <= 0)
        {
            Die();
        }
    }
    /**
   * Enemy was destroyed by tower
   */
    private void Die()
    {
        Player.money += moneyValue;
        Destroy(gameObject);
    }

}
