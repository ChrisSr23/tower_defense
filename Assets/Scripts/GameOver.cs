using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameOver : MonoBehaviour
{
    public TextMeshProUGUI UIText;
    public Image panel;
    public void Retry()
    {
 
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnEnable()
    {
        
        if(WaveSpawner.wonGame)
        {
            //display won-screen
            UIText.text = "You won!";
            panel.color = new Color(6 / 255f, 123 / 255f, 5 / 255f, 0.8f);
        }
        else
        {
            //display game over screen
            UIText.text = "Game Over!";
            panel.color = new Color(123 / 255f, 5 / 255f, 28 / 255f, 0.8f);

        }
    }


}
