using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //player stats
    public static float health;
    public static float money;
    public static float startMoney = 200;
    public static float startHealth = 10;
    // Start is called before the first frame update
    void Start()
    {
        money = startMoney;
        health = startHealth;
    }

 
}
