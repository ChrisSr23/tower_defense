using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Collect all waypoints
 */
public class WayPoints : MonoBehaviour
{
    public static Transform[] wayPoints;

    private void Awake()
    {
        //Get all wayPoints from map
        wayPoints = new Transform[transform.childCount];
        for(int i = 0; i < wayPoints.Length; i++)
        {
           
            wayPoints[i]= transform.GetChild(i);
           
        }
    }
}
