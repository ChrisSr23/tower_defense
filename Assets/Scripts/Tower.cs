using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
 * Define tower behaviour
 */
public class Tower : MonoBehaviour
{

    private Transform target;

    [Header("Attributes")]
    public float range = 10f;
    public float fireRate = 1f;
    private float fireCountdown = 0f;
    public float cost;


    [Header("Setup Fields")]
    public string enemyTag = "Enemy";
    public Transform partToRotate;
    public float rotationSpeed = 10f;

    public GameObject bulletPrefab;
    public Transform firePoint;
  

    // Start is called before the first frame update
    void Start()
    {
        //Execute FindTarget-function after 0 seconds, then after every half second
        InvokeRepeating("FindTarget", 0f, 0.5f);
      
        if(gameObject.tag == "Standard")
        {
            cost = BuildHelper.standardCost;
        }
        else if(gameObject.tag == "Mage")
        {
            cost = BuildHelper.mageCost;
        }
  
    }

    // Update is called once per frame
    void Update()
    {
        if(target== null)
        {
            return;
        }

        //Rotate tower to enemy
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        //convert to "x,y,z - rotation"; "Lerp" for smooth transition from current Position(partToRotate) to new Position (lookRotation)
        Vector3 towerRotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * rotationSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, towerRotation.y, 0f);
        //shoot the enemy    
        if(fireCountdown <= 0f)
        {
            Shoot();
            fireCountdown = 1f / fireRate;
        }

        fireCountdown -= Time.deltaTime;

    }
    /**
     * Find the nearest enemy
     */
    void FindTarget()
    {
        //store all enemies in array
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);

        float smallestdistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        //loop through enemies
        foreach(GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            //find smallest distance between tower and enemy
            if(distanceToEnemy < smallestdistance)
            {
                smallestdistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }
        //check if an enemy was found and is inside range of tower
        if(nearestEnemy != null && smallestdistance <= range)
        {
            target = nearestEnemy.transform;
        }
        else { target = null; }
    }



    /**
     * Shoot the bullet
     */
    private void Shoot()
    {
       
        //Make Instance of the bullet
        GameObject bulletGO = (GameObject) Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Bullet bullet = bulletGO.GetComponent<Bullet>();

        if(bullet != null)
        {
            //commit the current target of the tower to the bullet instance
            bullet.GetTarget(target);
        }
    }

    /**
     * Draw circle inside the editor (NOT IN GAME) around the tower (display range)
     **/
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, range);
    }




}
