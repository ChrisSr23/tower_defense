using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{

    BuildHelper buildHelper;
    

    void Start()
    {
        buildHelper = BuildHelper.instance;
    }

    /**
     * Choose standard tower
     */
    public void BuyStandardTurret()
    {

        Debug.Log("Standard Turret purchased");
        buildHelper.SetTurretToBuild(buildHelper.standardTurretPref);
    }
    /**
    * Choose mage tower
    */
    public void BuyMageTurret()
    {
        Debug.Log("Mage Turret purchased");
        buildHelper.SetTurretToBuild(buildHelper.mageTurretPref);
    }


}
