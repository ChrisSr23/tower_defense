using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildHelper : MonoBehaviour
{
    //singelton of BuildHelper -> only one instance possible
    public static BuildHelper instance;
    public static float standardCost = 100;
    public static float mageCost = 200;

    private void Awake()
    {
        if(instance != null)
        {
            Debug.LogError("Too many BuildHelper in this scene!");
            return;
        }
        //when game starts assign the BuildHelper to the variable instance 
        instance = this;
    }

    public GameObject standardTurretPref;
    public GameObject mageTurretPref;

    private GameObject turretToBuild;

    public GameObject GetTurretToBuild()
    {
        return turretToBuild;
    }
   
    public void SetTurretToBuild(GameObject turret)
    {
        turretToBuild = turret;
    }


}
