using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
/**
 * Build towers
 */
public class Platform : MonoBehaviour
{

    BuildHelper buildHelper;

    public Color hoverColor;
    public Vector3 positionOffset;

    private GameObject turretOnPlatform;
    private GameObject turretPreview;
    private Renderer currRenderer;
    private Color normalColor;

    public static Tower towerInstance;
    public DisplayRange circleRange;

    private void Start()
    {
        buildHelper = BuildHelper.instance;
        circleRange = FindObjectOfType<DisplayRange>();

        //Get renderer component of current object (for color ...)
        currRenderer = GetComponent<Renderer>();
        normalColor = currRenderer.material.color;
    }

    /**
     * User hovers ofer platform
     */
    private void OnMouseEnter()
    {
        //Is user hovering over button/UI Element?
        if(EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if (buildHelper.GetTurretToBuild() == null)
        {
            return;
        }
        //check if there is already a tower
                if (turretOnPlatform != null)
        {
            Debug.Log("No Preview Needed!");
            return;
        }
        //check for buying phase
        if (!HUD.buyPhase)
        {
            return;
        }
        //highlight currently selected platform for hover interaction
        currRenderer.material.color = hoverColor;
        //give previev of tower
        GameObject turretToBuild = buildHelper.GetTurretToBuild();
        turretPreview = (GameObject)Instantiate(turretToBuild, transform.position + positionOffset, transform.rotation);
        //access current instance of the tower GameObject
        towerInstance = FindObjectOfType<Tower>();
        //create a circle around the tower to display his shooting-range
        if(towerInstance != null)
        {
            Debug.Log("Create Circle");
            circleRange.createCircle(towerInstance.range, transform.position + new Vector3(0,2,0));
        }
        


    }
    /**
     * User leaves platform with the mouse
     */
    private void OnMouseExit()
    {
        //add old color
        currRenderer.material.color = normalColor;
        //destroy tower-instance and circle(shooting-range)
        Destroy(turretPreview);
        circleRange.deleteCircle();
    }

    /**
     * Mouse click to place tower
     */
    private void OnMouseDown()
    {
        //Is user over button/UI Element?
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        if (buildHelper.GetTurretToBuild() == null)
        {
            return;
        }
        if(turretOnPlatform != null)
        {
            Debug.Log("You can not build here!");
            return;
        }
        if (!HUD.buyPhase)
        {
            return;
        }
        //get the prefab to build
        GameObject turretToBuild = buildHelper.GetTurretToBuild();
        //access current instance of the tower GameObject
        towerInstance = FindObjectOfType<Tower>();
       
        //check if player can afford tower
        if (Player.money < towerInstance.cost)
        {
            Debug.Log("No money!");
            return;
        }
        else
        {
            //build tower & create circle(shooting-range) 
            Player.money -= towerInstance.cost;
            turretOnPlatform = (GameObject)Instantiate(turretToBuild, transform.position + positionOffset, transform.rotation);
            circleRange.createCircle(towerInstance.range, transform.position + new Vector3(0, 2, 0));
        }
        
    }

}
